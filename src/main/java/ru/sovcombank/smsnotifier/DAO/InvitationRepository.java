package ru.sovcombank.smsnotifier.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.sovcombank.smsnotifier.Entity.Invitation;

import java.util.Date;
import java.util.List;

public interface InvitationRepository extends JpaRepository<Invitation, Long> {


    List<Invitation> findAllByCreatedOnAndApiid(Date createdOn, int apiid);

}
