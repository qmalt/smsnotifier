package ru.sovcombank.smsnotifier.utils;

public class Translit {

    public static String cyr2lat(char ch){
        switch (ch){
            case 'а': return "a";
            case 'б': return "b";
            case 'в': return "v";
            case 'г': return "g";
            case 'д': return "d";
            case 'е': return "e";
            case 'ё': return "je";
            case 'ж': return "zh";
            case 'з': return "z";
            case 'и': return "i";
            case 'й': return "y";
            case 'к': return "k";
            case 'л': return "l";
            case 'м': return "m";
            case 'н': return "n";
            case 'о': return "o";
            case 'п': return "p";
            case 'р': return "r";
            case 'с': return "s";
            case 'т': return "t";
            case 'у': return "u";
            case 'ф': return "f";
            case 'х': return "kh";
            case 'ц': return "c";
            case 'ч': return "ch";
            case 'ш': return "sh";
            case 'щ': return "jsh";
            case 'ъ': return "hh";
            case 'ы': return "ih";
            case 'ь': return "jh";
            case 'э': return "eh";
            case 'ю': return "ju";
            case 'я': return "ja";

            case 'А': return "A";
            case 'Б': return "B";
            case 'В': return "V";
            case 'Г': return "G";
            case 'Д': return "D";
            case 'Е': return "E";
            case 'Ё': return "Je";
            case 'Ж': return "Zh";
            case 'З': return "Z";
            case 'И': return "I";
            case 'Й': return "Y";
            case 'К': return "K";
            case 'Л': return "L";
            case 'М': return "M";
            case 'Н': return "N";
            case 'О': return "O";
            case 'П': return "P";
            case 'Р': return "R";
            case 'С': return "S";
            case 'Т': return "T";
            case 'У': return "U";
            case 'Ф': return "F";
            case 'Х': return "Kh";
            case 'Ц': return "C";
            case 'Ч': return "Ch";
            case 'Ш': return "Sh";
            case 'Щ': return "Jsh";
            case 'Ъ': return "Hh";
            case 'Ы': return "Ih";
            case 'Ь': return "Jh";
            case 'Э': return "Eh";
            case 'Ю': return "Ju";
            case 'Я': return "Ja";
            default: return "";
        }
    }

    public static String cyr2lat(String s){
        StringBuilder sb = new StringBuilder(s.length()*3);
        for(char ch: s.toCharArray()){
            if (StringUtils.isLatinTransformable(ch))
                sb.append(cyr2lat(ch));
            else
                sb.append(ch);
        }
        sb.trimToSize();
        return sb.toString();
    }
}