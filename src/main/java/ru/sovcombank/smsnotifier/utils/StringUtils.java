package ru.sovcombank.smsnotifier.utils;

import java.util.regex.Pattern;

public class StringUtils {

    private static Pattern russianAlphabet = Pattern.compile("[\\x{410}-\\x{450}|\\x{401}|\\x{451}]");

    public static boolean isLatinTransformable(char ch){
        return russianAlphabet.matcher(String.valueOf(ch)).matches();
    }

}
