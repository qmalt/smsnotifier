package ru.sovcombank.smsnotifier.DTO;


import ru.sovcombank.smsnotifier.utils.Translit;
import ru.sovcombank.smsnotifier.annotation.GSM7Encoding;
import ru.sovcombank.smsnotifier.annotation.NoDuplicates;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class InviteRequestDTO implements Serializable {

    @NotNull(message = "401")
    @NotEmpty(message = "401")
    @Size(max = 16, message = "402")
    @NoDuplicates
    private List<String> phones;

    @GSM7Encoding
    @Size(max = 128, message = "407")
    private String msg;

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = Translit.cyr2lat(msg);
    }


}
