package ru.sovcombank.smsnotifier;

public enum Messages {
    PHONE_NUMBERS_INVALID,
    PHONE_NUMBERS_EMPTY,
    MESSAGE_EMPTY,
    MESSAGE_INVALID,

    INTERNAL
}
