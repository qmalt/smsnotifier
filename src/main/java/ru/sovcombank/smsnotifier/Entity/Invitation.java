package ru.sovcombank.smsnotifier.Entity;


import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Entity
@Validated
public class Invitation extends AbstractEntity {

    @Pattern(regexp = "7[\\d]{10}", message = "400")
    private String phone;

    private Invitation(){}

    public String getPhone() {
        return phone;
    }

    public static Builder builder(){
        return new Invitation().new Builder();
    }

    public class Builder {

        private Builder(){}

        public Builder setPhone(String phone) {
            Invitation.this.phone = phone;
            return this;
        }

        @Valid
        public Invitation build(){
            return Invitation.this;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invitation that = (Invitation) o;
        return Objects.equals(phone, that.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phone);
    }
}
