package ru.sovcombank.smsnotifier;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.ResponseEntity;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sovcombank.smsnotifier.DAO.InvitationRepository;
import ru.sovcombank.smsnotifier.DTO.InviteRequestDTO;
import ru.sovcombank.smsnotifier.Entity.Invitation;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping("/invitations")
public class InvitationController {


    @Autowired
    public InvitationRepository repository;

    Converter<InviteRequestDTO, List<Invitation>> toInvitationConverter = source -> {
        if (source == null || source.getPhones().isEmpty()) return new LinkedList<>();
        return source.getPhones().stream()
                .map(phone -> Invitation.builder()
                        .setPhone(phone)
                        .build())
                .collect(Collectors.toList());
    };

    @PostMapping
    @Transactional
    public ResponseEntity<Void> sendInvitations(@RequestBody @Valid InviteRequestDTO request) throws ConstraintViolationException {
        List<Invitation> newInvitations = toInvitationConverter.convert(request);
        try {
            List<Invitation> persistedInvitations = repository.findAllByCreatedOnAndApiid(new Date(), 4);
            List<String> duplicateInvitations = newInvitations.stream()
                    .filter(persistedInvitations::contains)
                    .map(Invitation::getPhone)
                    .collect(Collectors.toList());
            if (!duplicateInvitations.isEmpty())
                throw new RuntimeException("Invitation already sent to number(s) "+ duplicateInvitations);
            if (persistedInvitations.size()<=128)
                repository.saveAll(newInvitations);
        } catch (Exception e){
            Throwable throwable = NestedExceptionUtils.getRootCause(e);
            if (throwable instanceof ConstraintViolationException)
                throw (ConstraintViolationException) NestedExceptionUtils.getRootCause(e);
            throw e;
        }
        return ResponseEntity.ok().build();
    }

}
