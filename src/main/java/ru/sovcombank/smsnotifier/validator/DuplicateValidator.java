package ru.sovcombank.smsnotifier.validator;

import ru.sovcombank.smsnotifier.annotation.NoDuplicates;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DuplicateValidator implements ConstraintValidator<NoDuplicates, List<String>> {

    @Override
    public boolean isValid(List<String> value, ConstraintValidatorContext context) {
        Set<String> set = new HashSet<>(value);
        return set.size()==value.size();

    }
}
