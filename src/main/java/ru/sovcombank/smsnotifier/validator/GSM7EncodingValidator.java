package ru.sovcombank.smsnotifier.validator;

import ru.sovcombank.smsnotifier.utils.Gsm7Alphabet;
import ru.sovcombank.smsnotifier.annotation.GSM7Encoding;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GSM7EncodingValidator implements ConstraintValidator<GSM7Encoding, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Gsm7Alphabet.isGsm7Eligible(value);
    }

}
