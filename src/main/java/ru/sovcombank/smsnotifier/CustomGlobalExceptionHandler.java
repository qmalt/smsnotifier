package ru.sovcombank.smsnotifier;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Locale;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {



    private MessageSource messageSource;



    @Autowired
    public CustomGlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleException1(Exception ex, WebRequest request) {

        String body = String.format("%s %s: %s", HttpStatus.INTERNAL_SERVER_ERROR, Messages.INTERNAL, ex.getMessage());

        return new ResponseEntity<>(body, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

    }


    @ExceptionHandler({ ConstraintViolationException.class })
    protected ResponseEntity<Object> handleException(ConstraintViolationException ex, WebRequest request) {

        String errorMsg = ex.getConstraintViolations().iterator().next().getMessage();

        return response(errorMsg, new HttpHeaders(), request);

    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        String errorMsg = ex.getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .filter(key->key!=null && key.matches("40[1-7]|500"))
                .findFirst()
                .orElse("");

        if (errorMsg.isEmpty())
            return super.handleMethodArgumentNotValid(ex, headers, status, request);

        return response(errorMsg, headers, request);

    }

    private ResponseEntity<Object> response(String code, HttpHeaders headers, WebRequest request){

        Locale locale = request.getLocale();

        Messages shortMsg = null;


        HttpStatus status = HttpStatus.valueOf(Integer.valueOf(code));
        String series = status.is4xxClientError() ? "BAD_REQUEST" : " INTERNAL";
        String msg = messageSource.getMessage(code, null, locale);

        switch (code){
            case "400":
            case "402":
            case "403":
            case "404":{
                shortMsg = Messages.PHONE_NUMBERS_INVALID;
                break;
            }
            case "405":{
                shortMsg = Messages.MESSAGE_EMPTY;
                break;
            }
            case "401":{
                shortMsg = Messages.PHONE_NUMBERS_EMPTY;
                break;
            }
            case "406":
            case "407":{
                shortMsg = Messages.MESSAGE_INVALID;
                break;
            }
            default: {
                shortMsg = Messages.INTERNAL;
            }
        }

        String body = String.format("%s %s %s: %s", status.value(), series, shortMsg, msg);

        return new ResponseEntity<>(body, headers, status);
    }


}