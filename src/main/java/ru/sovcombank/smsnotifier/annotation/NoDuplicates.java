package ru.sovcombank.smsnotifier.annotation;

import ru.sovcombank.smsnotifier.validator.DuplicateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = DuplicateValidator.class)
@Documented
public @interface NoDuplicates {

    String message() default "404";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
